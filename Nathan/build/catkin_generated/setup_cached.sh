#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export PATH="/opt/ros/kinetic/bin:/home/chess/bin:/home/chess/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
export PWD="/home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/build"
export ROS_PACKAGE_PATH="/home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/src:/opt/ros/kinetic/share"