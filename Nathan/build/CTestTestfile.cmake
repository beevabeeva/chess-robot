# CMake generated Testfile for 
# Source directory: /home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/src
# Build directory: /home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(arbotix_ros-turtlebot2i/arbotix)
subdirs(arbotix_ros-turtlebot2i/arbotix_controllers)
subdirs(arbotix_ros-turtlebot2i/arbotix_firmware)
subdirs(arbotix_ros-turtlebot2i/arbotix_python)
subdirs(arbotix_ros-turtlebot2i/arbotix_sensors)
subdirs(arbotix_ros-turtlebot2i/arbotix_msgs)
subdirs(turtlebot_arm-kinetic-devel/turtlebot_arm)
subdirs(turtlebot_arm-kinetic-devel/turtlebot_arm_bringup)
subdirs(turtlebot_arm-kinetic-devel/turtlebot_arm_description)
subdirs(turtlebot_arm-kinetic-devel/turtlebot_arm_moveit_demos)
subdirs(turtlebot_arm-kinetic-devel/turtlebot_arm_kinect_calibration)
subdirs(turtlebot_arm-kinetic-devel/turtlebot_arm_ikfast_plugin)
subdirs(turtlebot_arm-kinetic-devel/turtlebot_arm_block_manipulation)
subdirs(turtlebot_arm-kinetic-devel/turtlebot_arm_moveit_config)
subdirs(turtlebot_arm-kinetic-devel/turtlebot_arm_object_manipulation)
