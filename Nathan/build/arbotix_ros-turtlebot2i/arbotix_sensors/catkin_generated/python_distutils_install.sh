#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/src/arbotix_ros-turtlebot2i/arbotix_sensors"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/install/lib/python2.7/dist-packages:/home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/build" \
    "/usr/bin/python" \
    "/home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/src/arbotix_ros-turtlebot2i/arbotix_sensors/setup.py" \
    build --build-base "/home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/build/arbotix_ros-turtlebot2i/arbotix_sensors" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/install" --install-scripts="/home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/install/bin"
