
"use strict";

let InteractiveBlockManipulationGoal = require('./InteractiveBlockManipulationGoal.js');
let BlockDetectionGoal = require('./BlockDetectionGoal.js');
let PickAndPlaceFeedback = require('./PickAndPlaceFeedback.js');
let BlockDetectionFeedback = require('./BlockDetectionFeedback.js');
let InteractiveBlockManipulationAction = require('./InteractiveBlockManipulationAction.js');
let BlockDetectionAction = require('./BlockDetectionAction.js');
let PickAndPlaceAction = require('./PickAndPlaceAction.js');
let BlockDetectionActionResult = require('./BlockDetectionActionResult.js');
let InteractiveBlockManipulationFeedback = require('./InteractiveBlockManipulationFeedback.js');
let BlockDetectionActionFeedback = require('./BlockDetectionActionFeedback.js');
let InteractiveBlockManipulationResult = require('./InteractiveBlockManipulationResult.js');
let PickAndPlaceActionGoal = require('./PickAndPlaceActionGoal.js');
let PickAndPlaceResult = require('./PickAndPlaceResult.js');
let PickAndPlaceActionFeedback = require('./PickAndPlaceActionFeedback.js');
let BlockDetectionResult = require('./BlockDetectionResult.js');
let PickAndPlaceActionResult = require('./PickAndPlaceActionResult.js');
let InteractiveBlockManipulationActionFeedback = require('./InteractiveBlockManipulationActionFeedback.js');
let InteractiveBlockManipulationActionGoal = require('./InteractiveBlockManipulationActionGoal.js');
let BlockDetectionActionGoal = require('./BlockDetectionActionGoal.js');
let InteractiveBlockManipulationActionResult = require('./InteractiveBlockManipulationActionResult.js');
let PickAndPlaceGoal = require('./PickAndPlaceGoal.js');

module.exports = {
  InteractiveBlockManipulationGoal: InteractiveBlockManipulationGoal,
  BlockDetectionGoal: BlockDetectionGoal,
  PickAndPlaceFeedback: PickAndPlaceFeedback,
  BlockDetectionFeedback: BlockDetectionFeedback,
  InteractiveBlockManipulationAction: InteractiveBlockManipulationAction,
  BlockDetectionAction: BlockDetectionAction,
  PickAndPlaceAction: PickAndPlaceAction,
  BlockDetectionActionResult: BlockDetectionActionResult,
  InteractiveBlockManipulationFeedback: InteractiveBlockManipulationFeedback,
  BlockDetectionActionFeedback: BlockDetectionActionFeedback,
  InteractiveBlockManipulationResult: InteractiveBlockManipulationResult,
  PickAndPlaceActionGoal: PickAndPlaceActionGoal,
  PickAndPlaceResult: PickAndPlaceResult,
  PickAndPlaceActionFeedback: PickAndPlaceActionFeedback,
  BlockDetectionResult: BlockDetectionResult,
  PickAndPlaceActionResult: PickAndPlaceActionResult,
  InteractiveBlockManipulationActionFeedback: InteractiveBlockManipulationActionFeedback,
  InteractiveBlockManipulationActionGoal: InteractiveBlockManipulationActionGoal,
  BlockDetectionActionGoal: BlockDetectionActionGoal,
  InteractiveBlockManipulationActionResult: InteractiveBlockManipulationActionResult,
  PickAndPlaceGoal: PickAndPlaceGoal,
};
