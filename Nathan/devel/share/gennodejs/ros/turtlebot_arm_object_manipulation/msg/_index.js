
"use strict";

let MoveToTargetFeedback = require('./MoveToTargetFeedback.js');
let MoveToTargetActionResult = require('./MoveToTargetActionResult.js');
let UserCommandActionResult = require('./UserCommandActionResult.js');
let UserCommandFeedback = require('./UserCommandFeedback.js');
let PickAndPlaceFeedback = require('./PickAndPlaceFeedback.js');
let InteractiveManipActionGoal = require('./InteractiveManipActionGoal.js');
let ObjectDetectionActionGoal = require('./ObjectDetectionActionGoal.js');
let ObjectDetectionAction = require('./ObjectDetectionAction.js');
let InteractiveManipActionFeedback = require('./InteractiveManipActionFeedback.js');
let PickAndPlaceAction = require('./PickAndPlaceAction.js');
let ObjectDetectionGoal = require('./ObjectDetectionGoal.js');
let UserCommandAction = require('./UserCommandAction.js');
let UserCommandResult = require('./UserCommandResult.js');
let PickAndPlaceActionGoal = require('./PickAndPlaceActionGoal.js');
let PickAndPlaceResult = require('./PickAndPlaceResult.js');
let ObjectDetectionActionResult = require('./ObjectDetectionActionResult.js');
let MoveToTargetResult = require('./MoveToTargetResult.js');
let InteractiveManipFeedback = require('./InteractiveManipFeedback.js');
let InteractiveManipActionResult = require('./InteractiveManipActionResult.js');
let ObjectDetectionActionFeedback = require('./ObjectDetectionActionFeedback.js');
let MoveToTargetGoal = require('./MoveToTargetGoal.js');
let PickAndPlaceActionFeedback = require('./PickAndPlaceActionFeedback.js');
let InteractiveManipGoal = require('./InteractiveManipGoal.js');
let PickAndPlaceActionResult = require('./PickAndPlaceActionResult.js');
let MoveToTargetActionGoal = require('./MoveToTargetActionGoal.js');
let InteractiveManipAction = require('./InteractiveManipAction.js');
let MoveToTargetAction = require('./MoveToTargetAction.js');
let ObjectDetectionResult = require('./ObjectDetectionResult.js');
let InteractiveManipResult = require('./InteractiveManipResult.js');
let MoveToTargetActionFeedback = require('./MoveToTargetActionFeedback.js');
let UserCommandGoal = require('./UserCommandGoal.js');
let PickAndPlaceGoal = require('./PickAndPlaceGoal.js');
let ObjectDetectionFeedback = require('./ObjectDetectionFeedback.js');
let UserCommandActionFeedback = require('./UserCommandActionFeedback.js');
let UserCommandActionGoal = require('./UserCommandActionGoal.js');

module.exports = {
  MoveToTargetFeedback: MoveToTargetFeedback,
  MoveToTargetActionResult: MoveToTargetActionResult,
  UserCommandActionResult: UserCommandActionResult,
  UserCommandFeedback: UserCommandFeedback,
  PickAndPlaceFeedback: PickAndPlaceFeedback,
  InteractiveManipActionGoal: InteractiveManipActionGoal,
  ObjectDetectionActionGoal: ObjectDetectionActionGoal,
  ObjectDetectionAction: ObjectDetectionAction,
  InteractiveManipActionFeedback: InteractiveManipActionFeedback,
  PickAndPlaceAction: PickAndPlaceAction,
  ObjectDetectionGoal: ObjectDetectionGoal,
  UserCommandAction: UserCommandAction,
  UserCommandResult: UserCommandResult,
  PickAndPlaceActionGoal: PickAndPlaceActionGoal,
  PickAndPlaceResult: PickAndPlaceResult,
  ObjectDetectionActionResult: ObjectDetectionActionResult,
  MoveToTargetResult: MoveToTargetResult,
  InteractiveManipFeedback: InteractiveManipFeedback,
  InteractiveManipActionResult: InteractiveManipActionResult,
  ObjectDetectionActionFeedback: ObjectDetectionActionFeedback,
  MoveToTargetGoal: MoveToTargetGoal,
  PickAndPlaceActionFeedback: PickAndPlaceActionFeedback,
  InteractiveManipGoal: InteractiveManipGoal,
  PickAndPlaceActionResult: PickAndPlaceActionResult,
  MoveToTargetActionGoal: MoveToTargetActionGoal,
  InteractiveManipAction: InteractiveManipAction,
  MoveToTargetAction: MoveToTargetAction,
  ObjectDetectionResult: ObjectDetectionResult,
  InteractiveManipResult: InteractiveManipResult,
  MoveToTargetActionFeedback: MoveToTargetActionFeedback,
  UserCommandGoal: UserCommandGoal,
  PickAndPlaceGoal: PickAndPlaceGoal,
  ObjectDetectionFeedback: ObjectDetectionFeedback,
  UserCommandActionFeedback: UserCommandActionFeedback,
  UserCommandActionGoal: UserCommandActionGoal,
};
