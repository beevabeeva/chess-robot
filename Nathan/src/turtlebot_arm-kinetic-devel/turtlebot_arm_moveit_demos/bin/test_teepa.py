#!/usr/bin/env python

import sys
import copy
import rospy
import time
import moveit_commander
import moveit_msgs.msg
from std_msgs.msg import Float64
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from sensor_msgs.msg import JointState
from moveit_commander.conversions import pose_to_list


def tester():
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    rate = rospy.Rate(1) # 10hz

    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()
    group_name = "arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)
    planning_frame = move_group.get_planning_frame()

    i = 0
    while not rospy.is_shutdown():

        if i%2 == 0:
            grip = 0.0
        else:
            grip = -1.65


        joint_goal = move_group.get_current_joint_values()
        print joint_goal
        rate.sleep()

        if i%2 == 0:


    # # halfway position
            joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
            move_group.go(joint_goal, wait=True)
            move_group.stop()
            rate.sleep()

    # right up position
            joint_goal = [0,0,0,0,0]
            move_group.go(joint_goal, wait=True)
            move_group.stop()
            rate.sleep()

            gripper.publish(grip)
            rate.sleep()


        else:

            gripper.publish(grip)
            rate.sleep()

    # halfway position
            joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
            move_group.go(joint_goal, wait=True)
            move_group.stop()
            rate.sleep()

    # down state
            joint_goal = [-1.6106798272799234, -2.1373465644539933, 0.260776733940559, 0.19430423313218123, 0.0]
            move_group.go(joint_goal, wait=True)
            move_group.stop()




# right up position
#         joint_goal = [0,0,0,0,0]
#         move_group.go(joint_goal, wait=True)
#         move_group.stop()
#         rate.sleep()

# # halfway position
#         joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
#         move_group.go(joint_goal, wait=True)
#         move_group.stop()
#         rate.sleep()

# # down state
#         joint_goal = [-1.6106798272799234, -2.1373465644539933, 0.260776733940559, 0.19430423313218123, 0.0]
#         move_group.go(joint_goal, wait=True)
#         move_group.stop()

        i = i + 1


        # gripper.publish(-1.6)
        rate.sleep()
        
        break # that's all folks!



if __name__ == '__main__':

    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('test', anonymous=True)

    try:
        tester()
    except rospy.ROSInterruptException:
        pass








# moveit_commander.roscpp_initialize(sys.argv)
# rospy.init_node('move_group_python_interface_tutorial', anonymous=True)

# ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
# ## kinematic model and the robot's current joint states
# robot = moveit_commander.RobotCommander()

# ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
# ## for getting, setting, and updating the robot's internal understanding of the
# ## surrounding world:
# scene = moveit_commander.PlanningSceneInterface()

# ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
# ## to a planning group (group of joints).  In this tutorial the group is the primary
# ## arm joints in the Panda robot, so we set the group's name to "panda_arm".
# ## If you are using a different robot, change this value to the name of your robot
# ## arm planning group.
# ## This interface can be used to plan and execute motions:
# group_name = "arm"
# move_group = moveit_commander.MoveGroupCommander(group_name)

# ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
# ## trajectories in Rviz:
# display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',moveit_msgs.msg.DisplayTrajectory,queue_size=20)





# planning_frame = move_group.get_planning_frame()

# rate = rospy.Rate(10)
# # move_group = self.move_group

# joint_goal = move_group.get_current_joint_values()
# print joint_goal
# rate.sleep()

# joint_goal = [0,0,0,0,0]
# move_group.go(joint_goal, wait=True)
# move_group.stop()
# rate.sleep()


# joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
# move_group.go(joint_goal, wait=True)
# move_group.stop()
# rate.sleep()


# joint_goal = [-1.6106798272799234, -2.1373465644539933, 0.260776733940559, 0.19430423313218123, 0.0]
# move_group.go(joint_goal, wait=True)
# move_group.stop()
