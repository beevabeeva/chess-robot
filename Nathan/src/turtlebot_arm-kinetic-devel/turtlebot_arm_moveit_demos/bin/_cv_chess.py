#!/usr/bin/env python

# r,n,b,q,k,b,n,r
# p,p,p,p,p,p,p,p
# 0,0,0,0,0,0,0,0
# 0,0,0,0,0,0,0,0
# 0,0,0,0,0,0,0,0
# 0,0,0,0,0,0,0,0
# P,P,P,P,P,P,P,P
# R,N,B,Q,K,B,N,R

# NOTE: when starting a new game, make sure the above starting state of the board is in board.txt


# Vision
import os
import sys

ros_path = '/opt/ros/kinetic/lib/python2.7/dist-packages'

if ros_path in sys.path:

    sys.path.remove(ros_path)

import cv2

sys.path.append('/opt/ros/kinetic/lib/python2.7/dist-packages')

# import cv2
import skimage 
import numpy as np
from skimage import feature
import matplotlib.pyplot as plt
from matplotlib import cm
from skimage.feature import canny
from skimage import filters
from scipy import ndimage
from operator import itemgetter
from stockfish import Stockfish
from centres import q


def see(im):
    cv2.imshow('image',im)
    cv2.waitKey(0)
    cv2.destroyAllWindows()



def pboard(board):
    teepa=False
    for i in range (0, len(board)):
        if(teepa):
            break
        for j in range (0, len(board)):
            if board[i][j]=='pink' or board[i][j]=='blue':
                teepa=True
                break
        
    
    if(teepa): #colour board
        for i in range(8):
            row = []
            for j in range(8):
                if(str(board[i][j])=='0'):
                    row.append(str(board[i][j]) +'    ')

                else:
                    row.append(str(board[i][j]) +' ')
            print('[%s]' % (' '.join(row)))
    else:# FEN board
        for i in range(8):
            row = []
            for j in range(8):
                row.append(str(board[i][j]) +' ')
            print('[%s]' % (' '.join(row)))






def get_frame():
	resource = 0
	cap = cv2.VideoCapture(resource)

	# if not cap.isOpened():
	#     exit(0)

	i = 0
	while i < 20:
	    i = i + 1
	    rval, frame = cap.read()
	return frame





def get_board_colours(img, squares):
    R = img[:,:,0]
    G = img[:,:,1]
    B = img[:,:,2]
    
#     se = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(99,99))
    se = cv2.getStructuringElement(cv2.MORPH_RECT,(9,9))
    r1 = cv2.morphologyEx(R, cv2.MORPH_OPEN, se)
    g1 = cv2.morphologyEx(G, cv2.MORPH_OPEN, se)
    b1 = cv2.morphologyEx(B, cv2.MORPH_OPEN, se)
    
    img[:,:,0] = r1
    img[:,:,1] = g1
    img[:,:,2] = b1
    
    i = 0
    board = []
    temp = []
    for square in squares:
        
        blue = False
        pink = False
        
        for x in [square[1] - 5, square[1], square[1] + 5]:
            for y in [square[0] - 5, square[0], square[0] + 5]:
                pixel = img[x, y]
                r = pixel[0]
                g = pixel[1]
                b = pixel[2]
        
                if r > g and b > 100 and r/g > 1.1:
                    cv2.putText(img, "p", (square[0], square[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.7, 0, 2)
#                     temp.append("pink")
                    pink = True
                elif r > b and r > 100 and r/g > 1.1:
                    cv2.putText(img, "b", (square[0], square[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.7, 0, 2)
                    blue = True
#                     temp.append("blue")
#                 else:
#                     temp.append(0)
            
        if pink:
            temp.append("pink")
        elif blue:
            temp.append("blue")
        else:
            temp.append(0)
    
        i = i + 1
        if i%8 == 0:
            board.append(temp)
            temp = []
            
    # see(img)
    return board















# q = [[441, 113], [441, 154], [443, 199], [444, 243], [445, 287], [447, 332], [447, 378], [449, 422],
#   [399, 114], [398, 155], [400, 199], [400, 242], [401, 287], [401, 332], [403, 377], [404, 425],
#   [354, 113], [355, 155], [355, 199], [356, 242], [356, 287], [357, 332], [356, 378], [357, 422], 
#   [312, 113], [311, 155], [311, 198], [311, 243], [312, 287], [312, 333], [312, 378], [312, 425], 
#   [268, 112], [268, 155], [267, 199], [267, 242], [267, 288], [267, 333], [266, 378], [267, 423], 
#   [224, 113], [223, 155], [223, 198], [222, 243], [221, 288], [221, 334], [221, 379], [220, 426],
#    [179, 111], [179, 155], [178, 199], [179, 242], [176, 288], [175, 333], [174, 380], [176, 424],
#     [139, 113], [133, 155], [135, 198], [130, 243], [133, 288], [122, 333], [131, 380], [121, 425]]










# Chess


def chess2colour(chess_board):
    output = []
    for i in range(8):
        l = []
        for j in range(8):
            p = chess_board[i][j]
            if p != 0:
                c=str(p)
                if c.isupper():
                    l.append("blue")
                else:
                    l.append("pink")
#             i = i + 1
            else:
                l.append(0)
        output.append(l)
    return output



def getMove(current_board, new_board):
    old=chess2colour(current_board)
    output = []
    diff = False
    d1 = False
    d2 = False
    for i in range(8):
        temp = []
        for j in range(8):
            if new_board[i][j] != old[i][j]:
                diff = True
                if new_board[i][j] == 0:
                    old_pos = [i,j]
                    temp.append("t")
                    d1 = True
                else:
                    new_pos = [i,j]
                    temp.append("t")
                    d2 = True
            else:
                temp.append(current_board[i][j])
        output.append(temp)
                    

    if d1 and d2:
#     Update board
        output[new_pos[0]][new_pos[1]] = current_board[old_pos[0]][old_pos[1]]
        output[old_pos[0]][old_pos[1]] = 0
    else:
        print("no move made")
        output[0][0] = "t"

    return output




def FEN(board, player, halfmove, fullmove):
    output = []
    end_of_zeros = True
    for i in range(8):
        row = []
        zero_counter = 0
        for j in range(8):
            if board[i][j] != 0:
                if not end_of_zeros:
                    row.append(str(zero_counter))
                    zero_counter = 0
                    end_of_zeros = True
                row.append(board[i][j])
            else:
                end_of_zeros = False
                zero_counter = zero_counter + 1
                if j == 7:
                    row.append(str(zero_counter))
                    end_of_zeros = True
        output.append("".join(row))
    config = "/".join(output)
    full = " ".join([config, player, "-", "-", str(halfmove), str(fullmove)])
    return(full)





# exit()

print("started vision")

file = open("board.txt", "r+")

# print(sys.argv[1])
if sys.argv[1] == "1":
	print("starting")
	start_board = [["r","n","b","q","k","b","n","r"],
				   ["p",'p',"p","p","p","p","p","p"],
				   [0,0,0,0,0,0,0,0],
				   [0,0,0,0,0,0,0,0],
				   [0,0,0,0,0,0,0,0],
				   [0,0,0,0,0,0,0,0],
				   ["P","P","P","P","P","P","P","P"],
				   ["R","N","B","Q","K","B","N","R"]]
else:
	start_board = []

	for line in file.readlines():
		temp = []
		for z in range(len(line)):
			if z % 2 == 0:
				if line[z].isdigit():
					temp.append(int(line[z]))
				else:
					temp.append(line[z])
		start_board.append(temp)


pboard(start_board)

# chess2colour(start_board)



# start = cv2.imread('/home/robotics/Pictures/move1.jpeg',1)
not_done = True
while not_done:
	current = get_frame()
	# see(current)
	b = get_board_colours(current, q)

	pboard(b)

	x = getMove(start_board, b)
	pboard(x)

	f = FEN(x, 'b', 0, 1)
	print(f)

	if "t" not in f:
		print("success")
		not_done = False


# exit()





# stockfish engine
sf = Stockfish('/usr/games/stockfish')

sf.set_fen_position(f)
bmove = sf.get_best_move()

print(bmove)

s1 = bmove[:2]
s2 = bmove[2:]

# print(s1)
# print(s2)

lookup = {"h": 7, "g": 6, "f": 5, "e": 4, "d": 3, "c": 2, "b": 1, "a": 0}

index = int((lookup[s1[0]]))

piece_type1 = (x[8-int(s1[1])][index])
x[8-int(s1[1])][index] = 0
# print(piece_type1)

if piece_type1 == "p" or piece_type1 == "P" or piece_type1 == "n" or piece_type1 == "N":
	grip1 = -2
else:
	grip1 = -1.7

index = int((lookup[s2[0]]))


piece_type2 = (x[8-int(s2[1])][index])
x[8-int(s2[1])][index] = piece_type1

pboard(x)
# print(piece_type2)

if piece_type2 == 0:
	grip2 = 0
elif piece_type2 == "p" or piece_type2 == "P" or piece_type1 == "n" or piece_type1 == "N":
	grip2 = -2
else:
	grip2 = -1.7


file.seek(0)
file.truncate()
for i in range(8):
	for j in range(8):
		file.write(str(x[i][j]))
		if j == 7:
			file.write("\n")
		else:
			file.write(",")

file.close()


# os.system("export ROS_NAMESPACE=arm1")
l1 = s1[0]
l2 = s2[0]

half1 = ["a", "b", "c", "d"]
half2 = ["e", "f", "g", "h"]

if (l1 in half1 and l2 in half2) or (l2 in half1 and l1 in half2):
	print("-----------------")
	print("-----------------")
	print("-----------------")
	print("Sorry, I can't do that :(")
	print("Please make the move for me")
	print("The move is " + l1 + " to " + l2)
	print("-----------------")
	print("-----------------")
	print("-----------------")
	exit()
elif l1 in half1:
	command = "rosrun turtlebot_arm_moveit_demos chess_movement2.py __ns:=arm2 " + s1 + " " + s2 + " " + str(grip1) + " " + str(grip2)
	command2 = "rosrun turtlebot_arm_moveit_demos press_button.py __ns:=arm1"
	os.system(command)
	os.system(command2)
else:
	command = "rosrun turtlebot_arm_moveit_demos chess_movement1.py __ns:=arm1 " + s1 + " " + s2 + " " + str(grip1) + " " + str(grip2)
	command2 = "rosrun turtlebot_arm_moveit_demos press_button.py __ns:=arm1"
	os.system(command)
	os.system(command2)



