# Vision
import os
import sys
import time

ros_path = '/opt/ros/kinetic/lib/python2.7/dist-packages'

if ros_path in sys.path:

    sys.path.remove(ros_path)

import cv2

sys.path.append('/opt/ros/kinetic/lib/python2.7/dist-packages')

# import cv2
import skimage 
import numpy as np
from skimage import feature
import matplotlib.pyplot as plt
from matplotlib import cm
from skimage.feature import canny
from skimage import filters
from scipy import ndimage
from operator import itemgetter
from stockfish import Stockfish


def see(im):
    cv2.imshow('image',im)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def get_squares(img):
# preprocessing
    ret, step1 = cv2.threshold(img[:,:,2], 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    step1 = 255 - step1
    step2 = img[:,:,2] + 10*step1
    ret, step3 = cv2.threshold(step2, 0, 255, cv2.THRESH_BINARY+12)
    step4 = img[:,:,2] + 100*step3
    
# first edges
    edges = canny(step4, 2, 1, 25)
    # plt.gray()
    # fig = plt.figure(figsize = (15,15))
    # plt.imshow(edges)
    # plt.show()

# closing to remove noise
    edgesv = edges.astype(np.uint8)*255
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(9,9))
    closing = cv2.morphologyEx(edgesv, cv2.MORPH_CLOSE, kernel)
#     see(closing)

# second edges
    edges2 = canny(closing, 0, 1, 25)
    # fig = plt.figure(figsize = (15,15))
    # plt.imshow(edges2)
    # plt.show()
    edges2v = edges2.astype(np.uint8)*255
    
# dilate edges to remove gaps
    se = cv2.getStructuringElement(cv2.MORPH_RECT,(9,9))
    dil = cv2.dilate(edges2v,se,iterations = 1)
    see(dil)

# get contours
    contours, hierarchy = cv2.findContours(dil, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contour_list = []
    cents = []
    i = 0
    for contour in contours:
        approx = cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True)
        area = cv2.contourArea(contour)
        (x, y, w, h) = cv2.boundingRect(approx)
        if w > 20 and h > 20 and w < 60 and h < 60 and area < 3000 and area > 800:
            cv2.putText(img, ".", (x + int(w/3), y + int(h/1.8)), cv2.FONT_HERSHEY_SIMPLEX, 0.7, 0, 2)
            contour_list.append(contour)
            cents.append([x + int(w/3), y + int(h/1.8)])
            i = i + 1
            
#     cv2.drawContours(img, contour_list,  -1, (0,0,0), 2)
    see(img)

# sort centres by coordinates
    cents.sort(key=itemgetter(1))
    cents.reverse()

    for i in range(8):
        qrow = cents[i*8:i*8+8]
        qrow.sort(key=itemgetter(0))
        qrow.reverse()
        cents[i*8:i*8+8] = qrow
        # cents.reverse()

    # for v in range(64):
    #     cv2.putText(img, str(v), (cents[v][0], cents[v][1]), cv2.FONT_HERSHEY_SIMPLEX, 0.7, 0, 2)
    # see(img)

    return cents








resource = 0

cap = cv2.VideoCapture(resource)

# if not cap.isOpened():
#     exit(0)

lq = 0
i = 0
while lq != 64:
    i = i + 1
    rval, frame = cap.read()
    if i > 15:
        print(frame.shape)
        # see(frame)


        # empty = cv2.imread('/home/robotics/Pictures/empty.jpeg',1)
        q = get_squares(frame)
        lq = len(q)
        print(lq)
        print(q)

file = open("centres.py", "w")

file.write("q=" + str(q))

file.close()