#!/usr/bin/env python

import sys
import copy
import rospy
import time
import moveit_commander
import moveit_msgs.msg
from std_msgs.msg import Float64
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from sensor_msgs.msg import JointState
from moveit_commander.conversions import pose_to_list




   

if __name__ == '__main__':
	f = open("down.txt", "a+")
	moveit_commander.roscpp_initialize(sys.argv)
	rospy.init_node('test', anonymous=True)
	group_name = "arm"
	move_group = moveit_commander.MoveGroupCommander(group_name)
	# f = open("cords.txt", "a+")
	# print("teepa")
	try:
		j = 0
		letters = ["h","g","f","e","d","c","b","a"]
		while not rospy.is_shutdown():
			index = (j % 8) + 1
			# l = letters[int(j/8)]
			l = "e"
			values=[]
			print("press enter to start saving coordinates for square " + str(l) + str(index))
			raw_input()
			for i in range(5):
				joint_goal = move_group.get_current_joint_values()
				print( str(i) + ") "+str(joint_goal))
				values.append(joint_goal)
				time.sleep(1)
			print("enter best run:")
			x = int(raw_input())
			f.write("down_" + str(l) + str(index) + "=" + str(values[x]) + "\n")
			print("Recorded "+str(x)+"): " + str(values[x]))
			j = j + 1
	except rospy.ROSInterruptException:
	    pass






