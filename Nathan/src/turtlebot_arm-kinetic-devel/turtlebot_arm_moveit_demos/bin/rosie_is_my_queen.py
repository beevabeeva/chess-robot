#!/usr/bin/env python

import sys
import copy
import rospy
import time
import math
import moveit_commander
import moveit_msgs.msg
from std_msgs.msg import Float64
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from sensor_msgs.msg import JointState
from moveit_commander.conversions import pose_to_list



def tester():
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    rate = rospy.Rate(1) # 10hz

    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()
    group_name = "arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)
    planning_frame = move_group.get_planning_frame()

    while not rospy.is_shutdown():


        joint_goal = move_group.get_current_joint_values()
        print joint_goal
        rate.sleep()


# # halfway position
        joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()

# right up position
        joint_goal = [0,0,0,0,0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()

        gripper.publish(grip)
        rate.sleep()


        gripper.publish(grip)
        rate.sleep()

# halfway position
        joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()

# down state
        joint_goal = [-1.6106798272799234, -2.1373465644539933, 0.260776733940559, 0.19430423313218123, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()


        # gripper.publish(-1.6)
        rate.sleep()
        
        break # that's all folks!



def hover(inp, move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        # joint_goal = [0,0,0,0,0]
        move_group.go(inp, wait=True)
        move_group.stop()
        rate.sleep()
        break





def upright(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal = [0,0,0,0,0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break


def halfway(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break


def downstate(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal =  [0.13294500161675557, -0.26589000323351114, -1.078899820812901, -1.6157930965728755, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break


def up1(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal =  [1.3550163626323164, 0.9306150113172891, -1.7640779060684875, -1.1504855909142309, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break

def move1(move_group):
    rate = rospy.Rate(1) # 10hz
    while not rospy.is_shutdown():
        joint_goal = [1.9072494462711473, -0.5675728915176873, -0.6084790458613044, -0.9101619341454805, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break

def down2(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal = [1.897022907685243, -0.7209709703062513, -0.66983827737673, -0.9050486648525283, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break

def grip(grip1):
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    grip_status = rospy.Subscriber('/joint_states',JointState)
    rate = rospy.Rate(1) # 10hz
    t = 0
    while not rospy.is_shutdown():
        while t < 2:
            gripper.publish(grip1)
            t = t + 1
            rate.sleep()
        print("hello teepa")
        break


def gripKnight():
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    grip_status = rospy.Subscriber('/joint_states',JointState)
    rate = rospy.Rate(1) # 10hz
    grip1=-0.5
    t = 0
    while not rospy.is_shutdown():
        while t < 2:
            gripper.publish(grip1)
            t = t + 1
            rate.sleep()
        print("hello teepa")
        break


def downstate_test(move_group):
    shoulder_lift = rospy.Publisher('/arm_shoulder_lift_joint/command', Float64, queue_size=10)
    rate = rospy.Rate(10) # 10hz
    print("rate" ,rate)
    while not rospy.is_shutdown():
        i = 0
        j=30
        position1 = 0
        while position1 < 0.7:
            position1 = 0.7*math.sin(math.pi*(0.01*i))
            # shoulder_lift.publish(position1)
            position2 = 0.7 - 0.7*math.exp(-i)
            pos3=i*0.7/50
            shoulder_lift.publish(position1)
            print(pos3)
            i = i + 1
            rate.sleep()
            rate = rospy.Rate(j) # 10hz
            print("sleep: ",j )
            j=j-0.5

        break

def release():
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    grip_status = rospy.Subscriber('/joint_states',JointState)
    rate = rospy.Rate(1) # 10hz
    grip1=0
    t = 0
    while not rospy.is_shutdown():
        while t < 2:
            gripper.publish(grip1)
            t = t + 1
            rate.sleep()
        print("hello teepa")
        break


def test():
    wrist_flex = rospy.Publisher('/arm_wrist_flex_joint/command', Float64, queue_size=10)

    rate = rospy.Rate(10) # 10hz

    i=0
    while not rospy.is_shutdown():
        while i < 1:
            # position1 = 0.57*abs(math.cos(math.pi*(0.1*i)))-0.25
            position1 = -(0.57+0.25)*i + 0.57
            
            # base swivel
            wrist_flex.publish(position1) 
            print(position1)

            if position1 < 0.2:
                i = i + 0.2
            else:
                i = i + 0.025
            rate.sleep()
        i = 0
        while i < 1:
            # position1 = 0.57*abs(math.cos(math.pi*(0.1*i)))-0.25
            position1 = +(0.57+0.25)*i + -0.14
            
            # base swivel
            wrist_flex.publish(position1) 
            print(position1)

            if position1 < 0.2:
                i = i + 0.2
            else:
                i = i + 0.025
            rate.sleep()
        break







if __name__ == '__main__':

    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('test', anonymous=True)
    group_name = "arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    move_group.set_max_velocity_scaling_factor(0.5);
    # move_group.set_max_acceleration_scaling_factor(0.1);


    hover_h1=[0.8883496254582463, 0.5675728915176873, -1.6260196351587797, -1.0328803971763318, 0.0]
    hover_h2=[0.6749515466696822, 0.8692557798018634, -1.7640779060684875, -1.1351457830353746, 0.0]
    hover_h3=[0.3937217355573146, 0.7874434711146292, -1.3141102082886993, -1.564660403643354, 0.0]
    hover_h4=[0.19941750242513337, 0.9919742428327146, -1.4266021327336464, -1.6464727123305882, 0.0]
    hover_h5=[-0.11249192444494703, 1.3243367468746037, -1.9532688699077165, -1.3345632854605078, 0.0]
    hover_h6=[-0.3834951969714103, 0.9612946270750019, -1.5851134808151626, -1.4266021327336464, 0.0]
    hover_h7=[-0.6187055844472087, 0.8641425105089112, -1.7333982903107745, -1.191391745257848, 0.0]
    hover_h8=[-0.8385761640441506, 0.521553467881118, -1.6464727123305882, -0.9255017420243369, 0.0]

    down_h1=[0.894822126266624, -0.4211003907093095, -0.894822126266624, -1.5023950177843112, 0.0]
    down_h2=[0.74005178931344428, -0.29145634969827183, -1.191391745257848, -1.4023950177843112, 0.0]
    down_h3=[0.4959871214163573, -0.04601942363656924, -1.4675082870772636, -1.4023950177843112, 0.0]
    down_h4=[0.2300971181828462, 0.18498384888989406, -1.6720390587953489, -1.426215563702156, 0.0]
    down_h5=[-0.10226538585904275, 0.10896442525332482, -1.5515859816235403, -1.4826215563702156, 0.0]
    down_h6=[-0.4039482741432189, -0.02045307717180855, -1.4726215563702156, -1.3823950177843112, 0.0]
    down_h7=[-0.6235923151542565, -0.2561165418194154, -1.122071361015561, -1.53950177843112, 0.0]
    down_h8=[-0.7674434711146292, -0.4511003907093095, -0.8550486648525283, -1.5023950177843112, 0.0]



    hover_g1=[0.7465373167710121, -0.260776733940559, -0.6391586616190172, -1.3856959783900293, 0.0]
    hover_g2=[0.5982525072754001, 0.04601942363656924, -0.9766344349538583, -1.3703561705111729, 0.0]
    hover_g3=[0.36304211979960177, 0.3528155812136975, -1.406149055561838, -1.2067315531367044, 0.0]
    hover_g4=[0.14317154020265985, 0.46530750565864454, -1.5442073264715455, -1.1402590523283267, 0.0]
    hover_g5=[-0.08692557798018634, 0.4908738521234052, -1.5800002115222105, -1.1351457830353746, 0.0]
    hover_g6=[-0.3016828882841761, 0.521553467881118, -1.6413594430376361, -1.037993666469284, 0.0]
    hover_g7=[-0.4908738521234052, 0.4192880820220753, -1.6618125202094447, -0.8794823183877677, 0.0]
    hover_g8=[-0.6596117387908257, 0.08692557798018634, -1.3959225169759335, -0.7567638553569164, 0.0]


    down_g1=[0.7860842395992035, -0.7266667371740702, -0.3999353955595762, -1.5732040539450822, 0.0]
    down_g2=[0.5880259686894959, -0.33236250404188894, -1.1351457830353746, -1.2732040539450822, 0.0]
    down_g3=[0.39883500485026674, -0.19941750242513337, -1.3089969389957472, -1.2732040539450822, 0.0]
    down_g4=[0.1891909638392291, -0.11760519373789917, -1.4010357862688856, -1.2732040539450822, 0.0]
    down_g5=[-0.05113269292952138, -0.10737865515199489, -1.416375594147742, -1.2732040539450822, 0.0]
    down_g6=[-0.29145634969827183, -0.27385115596037268, -1.2499030933393643, -1.2732040539450822, 0.0]
    down_g7=[-0.48064731353750095, -0.38656961899122397, -0.9862784759648959, -1.3732040539450822, 0.0]
    down_g8=[-0.654045392326065, -0.7164401985881659, -0.4357282806102412, -1.5932040539450822, 0.0]




    hover_f1=[0.6442719309119693, -0.3033657765683522, -0.351987057959694192, -1.3726215563702156, 0.0]
    hover_f2=[0.43974115919388385, -0.3266667371740702, -0.34032365676875048, -1.3493205957644978, 0.0]
    hover_f3=[0.26589000323351114, -0.3057605828304531, -0.34032365676875048, -1.5464727123305882, 0.0]
    hover_f4=[0.09203884727313848, -0.2046278899009317, -0.29145634969827183, -1.5771523280883012, 0.0]
    hover_f5=[-0.11249192444494703, -0.22440135131502743, -0.29656961899122397, -1.5822655973812533, 0.0]
    hover_f6=[-0.2016828882841761, -0.23974115919388385, -0.28122981111236756, -1.4771523280883012, 0.0]
    hover_f7=[-0.40508096707274026, -0.3768932757599745, -0.2300971181828462, -1.470453288694019, 0.0]
    hover_f8=[-0.5284790458613044, -0.4442719309119693, -0.23896442525332482, -1.4572817484913592, 0.0]


    down_f1=[0.6442719309119693, -0.8743690490948155, -0.4192880820220753, -1.262977515359178, 0.0]
    down_f2= [0.4917800064670223, -0.7414240474780599, -0.5317800064670223, -1.380582709097077, 0.0]
    down_f3 = [0.32326865838550605, -0.5170226961630325, -0.8051457830353746, -1.3249192444494702, 0.0]
    down_f4= [0.1591909638392291, -0.6346278899009317, -0.55119742428327146, -1.5783173232380345, 0.0]
    down_f5= [-0.0556634646476069, -0.50566346464760686, -0.8510357862688856, -1.385017420243369, 0.0]
    down_f6=  [-0.23521038747579834, -0.3681553890925539, -1.1504855909142309, -1.078899820812901, 0.0]
    down_f7= [-0.38440135131502743, -0.5062136600022616, -0.9408415499031934, -1.1504855909142309, 0.0]
    down_f8=[-0.5575728915176873, -0.9987055844472087, -0.1026538585904275, -1.4788027026300549, 0.0]



    hover_e1=[0.5675728915176873, -0.6027832789934856, -0.28122981111236756, -1.0488027026300549, 0.0]
    hover_e2=[0.43883500485026674, -0.4391586616190172, -0.29145634969827183, -1.2095794365706138, 0.0]
    hover_e3=[0.29032365676875048, -0.4493852002049215, -0.3016828882841761, -1.2555988602071832, 0.0]
    hover_e4=[0.13669903939428206, -0.3468932757599745, -0.3272492347489368, -1.257864246066226, 0.0]
    hover_e5=[-0.02158577010132992, -0.3864401985881659, -0.35792885050664963, -1.3032040539450822, 0.0]
    hover_e6=[-0.1854369260617026, -0.4275728915176873, -0.30679615757712825, -1.26809078465213, 0.0]
    hover_e7=[-0.31883500485026674, -0.5465373167710121, -0.17896442525332482, -1.1226538585904275, 0.0]
    hover_e8=[-0.4562136600022616, -0.6641425105089112, -0.12783173232380343, -0.9955017420243369, 0.0]


    down_e1=[0.6022330836388308, -0.9459548191961454, -0.4908738521234052, -0.9050486648525283, 0.0]
    down_e2=[0.4192880820220753, -0.9986732822269966, -0.18692557798018634, -1.2952429012182208, 0.0]
    down_e3=[0.30589000323351114, -1.0124273200045233, -0.05226538585904275, -1.487961364249072, 0.0]
    down_e4= [0.1380582709097077, -0.6096117387908257, -0.9306150113172891, -0.9306150113172891, 0.0]
    down_e5=[-0.050158577010132992, -0.5777994301035916, -0.9868609735397625, -0.9050486648525283, 0.0]
    down_e6=[-0.20566346464760686, -0.6391586616190172, -0.9050486648525283, -0.9050486648525283, 0.0]
    down_e7=[-0.3283819276784582, -0.9963107781851078, -0.1567638553569164, -1.35550486648525283, 0.0]
    down_e8=[-0.4808738521234052, -0.9959548191961454, -0.251553467881118, -1.15050486648525283, 0.0]



    downtest= [0.1380582709097077, -0.6096117387908257, -0.9306150113172891, -0.9306150113172891, 0.0]

# [-0.5675728915176873, -0.6187055844472087, -1.0226538585904275, -0.7488027026300549, 0.0]

    try:

        while True:
            hover([0,0,0,0,0], move_group)
            time.sleep(1)
            hover([1.1053723216212788, -1.1686732822269966, -0.9101619341454805, 0.573788666742054, 0.0],move_group)
            test()
            time.sleep(1)
        # hover(down_e8, move_group)
        # time.sleep(2)
        # grip(-1.7)
        # time.sleep(2)

        # hover(hover_e8,move_group)
        # time.sleep(2)
        # hover(hover_e1,move_group)
        # time.sleep(2)
        # hover(down_e1, move_group)
        # time.sleep(2)
        # grip(0)
        # time.sleep(2)

        # hover(hover_h1,move_group)
        # time.sleep(2)
        # hover(down_h1, move_group)
        # time.sleep(2)
        # grip(-2)
        # time.sleep(2)

        # hover(hover_h2,move_group)
        # time.sleep(2)
        # hover(down_h2, move_group)
        # time.sleep(2)

        # hover(hover_h3,move_group)
        # time.sleep(2)
        # hover(down_h3, move_group)
        # time.sleep(2)

        # hover(hover_h4,move_group)
        # time.sleep(2)
        # hover(down_h4, move_group)
        # time.sleep(2)

        # hover(hover_h5,move_group)
        # time.sleep(2)
        # hover(down_h5, move_group)
        # time.sleep(2)

        # hover(hover_h6,move_group)
        # time.sleep(2)
        # hover(down_h6, move_group)
        # time.sleep(2)

        # hover(hover_h7,move_group)
        # time.sleep(2)
        # hover(down_h7, move_group)
        # time.sleep(2)

        # hover(hover_h8,move_group)
        # time.sleep(2)
        # hover(down_h8, move_group)
        # time.sleep(2)
        # grip(0)
        


        # hover(hover_e4,move_group)
        # time.sleep(2)
        # hover(downtest,move_group)
        # time.sleep(2)
    except rospy.ROSInterruptException:
        pass





# [1.7333982903107745, 0.6238188537401608, -1.968608677786573, -0.9357282806102412, 0.0]
