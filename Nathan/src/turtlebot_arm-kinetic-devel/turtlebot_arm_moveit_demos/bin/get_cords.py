#!/usr/bin/env python

import sys
import copy
import rospy
import time
import moveit_commander
import moveit_msgs.msg
from std_msgs.msg import Float64
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from sensor_msgs.msg import JointState
from moveit_commander.conversions import pose_to_list



def tester():
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    rate = rospy.Rate(1) # 10hz

    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()
    group_name = "arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)
    planning_frame = move_group.get_planning_frame()

    while not rospy.is_shutdown():


        joint_goal = move_group.get_current_joint_values()
        print joint_goal
        rate.sleep()


# # halfway position
        joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()

# right up position
        joint_goal = [0,0,0,0,0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()

        gripper.publish(grip)
        rate.sleep()


        gripper.publish(grip)
        rate.sleep()

# halfway position
        joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()

# down state
        joint_goal = [-1.6106798272799234, -2.1373465644539933, 0.260776733940559, 0.19430423313218123, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()


        # gripper.publish(-1.6)
        rate.sleep()
        
        break # that's all folks!


def upright(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal = [0,0,0,0,0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break


def halfway(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break


def downstate(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal = [-1.6873788666742053, 0.7516505860639642, 0.4908738521234052, 1.5288675185926892, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break


def grip():
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    grip_status = rospy.Subscriber('/joint_states',JointState)
    rate = rospy.Rate(1) # 10hz
    grip1=-1.6
    t = 0
    while not rospy.is_shutdown():
        while t < 2:
            gripper.publish(grip1)
            t = t + 1
            rate.sleep()
        print("hello teepa")
        break


def gripKnight():
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    grip_status = rospy.Subscriber('/joint_states',JointState)
    rate = rospy.Rate(1) # 10hz
    grip1=-0.5
    t = 0
    while not rospy.is_shutdown():
        while t < 2:
            gripper.publish(grip1)
            t = t + 1
            rate.sleep()
        print("hello teepa")
        break


def release():
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    grip_status = rospy.Subscriber('/joint_states',JointState)
    rate = rospy.Rate(1) # 10hz
    grip1=0
    t = 0
    while not rospy.is_shutdown():
        while t < 2:
            gripper.publish(grip1)
            t = t + 1
            rate.sleep()
        print("hello teepa")
        break



if __name__ == '__main__':

    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('test', anonymous=True)
    group_name = "arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)


    try:
        while not rospy.is_shutdown():
            joint_goal = move_group.get_current_joint_values()
            print joint_goal
            time.sleep(1)
    except rospy.ROSInterruptException:
        pass






