��    ;      �              �     �     �     �  �   �  7   �  �     A   �  ;   �  �  !  :     e   Y     �     �     �     �     �     �  (   	     0	     E	     M	     ^	     r	  	   x	     �	     �	     �	  $   �	     �	     �	     �	     �	     

     
     )
     0
  D   6
  -   {
     �
     �
     �
     �
     �
     �
               "     /     5     ;     @     L     R  	   _     i     p     |     �  �  �     +     D     M    ^  =   l  P   �  P   �  Q   L    �  B   �  W   �     K     P     `     t     �     �  +   �     �     �                &     +     3     F     M  +   _     �     �     �     �     �     �  	   �     �  9     0   <  %   m  	   �     �     �     �     �     �                    "     (     9     E  	   N  
   X     c     h     x     �    %d byoyomi  %d byoyomis  (lost) %d move %d moves A delay timing style used for chess and designed by Bobby Fischer. Everytime the player makes a move, a delay is added to the clock. Defaults: 2 minutes per player, 10 second increment, results in a reasonably short chess game. A quick 15 minutes per player chess game, no increment. A regular board game. A clock goes down until time runs out. The counter is reset at every move. Default is to give 2 minutes per move. A regular chess game of 60 minutes per player, with no increment. A simple game clock to be used for Chess or any board game. A standard or "japanese" Go counter. A clock counts down until its time runs out and then enters overtime. Once in overtime the player has a specific number of overtime periods (or "Byo-yomi") to play. If the player moves within the chosen delay, the clock's time reverts to the top of the period and no periods are lost. If the player does not move within the period, the next period begins and the period count is reduced by 1. A player loses when his/her clock runs out of time and no more periods remain. A very fast chess game, 2 minutes per player no increment. Behave like an hourglass: the clock goes down on one side and up on the other, 60 seconds by default. Blue Board Change players settings Change time settings Default Delay:  Display the available keyboard shortcuts Enable/disable sound Fischer Full screen mode Go Standard Byoyomi Green Hourglass Keyboard shortcuts Left Lightning Chess More information about this software Number of Byo-yomi:  Number of players:  Pause Quick Chess Quit the Program Regular Chess Resume Right Selected player is blue, dead player is red, normal background black Set the number of players and starting player Set the starting time of clocks Start Start/pause game Starting player:  Theme Time limit:  Use the default theme _About _Full screen _Game _Help _New _Players... _Quit _Set time... _Settings _Sound game paused game resumed game running Project-Id-Version: 5.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-01-22 23:50-0500
PO-Revision-Date: 2013-04-07 19:26-0400
Last-Translator: Antoine Beaupré <anarcat@debian.org>
Language-Team: French
Language: fr_CA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-Language: French
X-Poedit-Country: CANADA
  %d byoyomi  %d byoyomis  (perdu) %d coup %d coups Un style d'horloge à délai pour les échecs conçue par Bobby
Fischer. Chaque fois qu'un joueur termine son tour, un délai est
ajouté à son horloge. 10 secondes de délai par défaut et un compteur
de départ de 2 minutes donne une partie d'une durée raisonnable. Une partie rapide, 15 minutes par jour, sans incrémentation. Une partie de table normale, 2 minutes par tour, remise à zéro à
chaque tour. Une partie d'échecs régulière de 60 minutes par joueur, sans délai spécial. Un chronomètre de jeu simple pour les jeux d'échecs ou tout autre jeu
de table. Une horloge de go standard ou "japonaise". L'horloge descend jusqu'à
ce que le temps arrive à zéro. À ce moment le joueur a un nombre de
périodes supplémentaires (le "byo-yomi") pour jouer. Si le joueur joue
durant le délai spécifié, l'horloge revient au délai de départ et
aucune période n'est perdue. Si le joueur ne joue pas durant la
période, la période suivante commence et le nombre de période est
réduit de 1. Le joueur perd lorsque son horloge n'a plus de temps et
qu'il n'y a plus de période disponible. Une partie très rapide, 5 minutes par jour, sans incrémentation. Comme un sablier: quand une horloge augmente, l'autre descend. 60
secondes par défaut. Bleu Partie de table Changer les joueurs Changer le temps des horloges Par défaut Délai:  Montrer les raccourcis claviers disponibles Activer/désactiver le son Échecs (Fischer) Mode plein écran Go Byoyomi Standard Vert Sablier Raccourcis clavier Gauche Échecs (éclair) Obtenir plus d'informations sur ce logiciel Nombre de Byo-yomi:  Nombre de joueurs:  Pause Échecs (rapide) Quitter le programme Échecs (normal) Reprendre Droite Le joueur actif bleu, le joueur mort rouge, le fond noir. Régler le nombre de joueur et le premier joueur Régler le temps initial des horloges Démarrer Démarrer/arrêter la partie Premier joueur:  Thème Limite de temps:  Utiliser le thème par défaut À propos... Plein écran _Jeu _Aide _Nouvelle partie _Joueurs... _Quitter _Temps... _Réglages _Son partie en pause partie reprise partie en cours 