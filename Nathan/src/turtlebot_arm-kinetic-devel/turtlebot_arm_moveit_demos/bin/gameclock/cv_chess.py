#!/usr/bin/env python

# Vision
import os
import sys


ros_path = '/opt/ros/kinetic/lib/python2.7/dist-packages/cv2.so'
if ros_path  in sys.path:

    sys.path.remove(ros_path)

import cv2

sys.path.append('/opt/ros/kinetic/lib/python2.7/dist-packages/cv2.so')

# import cv2
import skimage 
import numpy as np
from skimage import feature
import matplotlib.pyplot as plt
from matplotlib import cm
from skimage.feature import canny
from skimage import filters
from scipy import ndimage
from operator import itemgetter
from stockfish import Stockfish


def see(im):
    cv2.imshow('image',im)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def get_squares(img):
# preprocessing
    ret, step1 = cv2.threshold(img[:,:,2], 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    step1 = 255 - step1
    step2 = img[:,:,2] + 10*step1
    ret, step3 = cv2.threshold(step2, 0, 255, cv2.THRESH_BINARY+12)
    step4 = img[:,:,2] + 100*step3
    
# first edges
    edges = canny(step4, 2, 1, 25)
    # plt.gray()
    # fig = plt.figure(figsize = (15,15))
    # plt.imshow(edges)
    # plt.show()

# closing to remove noise
    edgesv = edges.astype(np.uint8)*255
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(9,9))
    closing = cv2.morphologyEx(edgesv, cv2.MORPH_CLOSE, kernel)
#     see(closing)

# second edges
    edges2 = canny(closing, 0, 1, 25)
    # fig = plt.figure(figsize = (15,15))
    # plt.imshow(edges2)
    # plt.show()
    edges2v = edges2.astype(np.uint8)*255
    
# dilate edges to remove gaps
    se = cv2.getStructuringElement(cv2.MORPH_RECT,(9,9))
    dil = cv2.dilate(edges2v,se,iterations = 1)
    # see(dil)

# get contours
    contours, hierarchy = cv2.findContours(dil, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contour_list = []
    cents = []
    i = 0
    for contour in contours:
        approx = cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True)
        area = cv2.contourArea(contour)
        (x, y, w, h) = cv2.boundingRect(approx)
        if w > 40 and h > 40 and w < 85 and h < 85 and area < 3000 and area > 1500:
            cv2.putText(img, str(i), (x + int(w/3), y + int(h/1.8)), cv2.FONT_HERSHEY_SIMPLEX, 0.7, 0, 2)
            contour_list.append(contour)
            cents.append([x + int(w/3), y + int(h/1.8)])
            i = i + 1
            
#     cv2.drawContours(empty, contour_list,  -1, (0,0,0), 2)
#     see(empty)

# sort centres by coordinates
    cents.sort(key=itemgetter(0))
    cents.reverse()

    for i in range(8):
        qrow = cents[i*8:i*8+8]
        qrow.sort(key=itemgetter(1))
        cents[i*8:i*8+8] = qrow
#     cents.reverse()

    return cents











print("gsidfgnsid")
empty = cv2.imread('/home/robotics/Nathan/src/turtlebot_arm-kinetic-devel/turtlebot_arm_moveit_demos/bin/gameclock/empty.jpeg',1)
see(empty)
q = get_squares(empty)
print(len(q))
print(q)




def pboard(board):
    teepa=False
    for i in range (0, len(board)):
        if(teepa):
            break
        for j in range (0, len(board)):
            if board[i][j]=='pink' or board[i][j]=='blue':
                teepa=True
                break
        
    
    if(teepa): #colour board
        for i in range(8):
            row = []
            for j in range(8):
                if(str(board[i][j])=='0'):
                    row.append(str(board[i][j]) +'    ')

                else:
                    row.append(str(board[i][j]) +' ')
            print('[%s]' % (' '.join(row)))
    else:# FEN board
        for i in range(8):
            row = []
            for j in range(8):
                row.append(str(board[i][j]) +' ')
            print('[%s]' % (' '.join(row)))











def get_board_colours(img, squares):
    R = img[:,:,0]
    G = img[:,:,1]
    B = img[:,:,2]
    
#     se = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(99,99))
    se = cv2.getStructuringElement(cv2.MORPH_RECT,(9,9))
    r1 = cv2.morphologyEx(R, cv2.MORPH_OPEN, se)
    g1 = cv2.morphologyEx(G, cv2.MORPH_OPEN, se)
    b1 = cv2.morphologyEx(B, cv2.MORPH_OPEN, se)
    
    img[:,:,0] = r1
    img[:,:,1] = g1
    img[:,:,2] = b1
    
    i = 0
    board = []
    temp = []
    for square in squares:
        pixel = img[square[1],square[0]]
        r = pixel[0]
        g = pixel[1]
        b = pixel[2]
        
        if r > g and b > 100:
            cv2.putText(img, "p", (square[0], square[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.7, 0, 2)
            temp.append("pink")
        elif r > b and r > g and r > 100:
            cv2.putText(img, "b", (square[0], square[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.7, 0, 2)
            temp.append("blue")
        else:
            temp.append(0)
        i = i + 1
        if i%8 == 0:
            board.append(temp)
            temp = []
            
    see(img)
    return board



















start = cv2.imread('/home/robotics/Nathan/src/turtlebot_arm-kinetic-devel/turtlebot_arm_moveit_demos/bin/gameclock/move1.jpeg',1)
b = get_board_colours(start, q)
pboard(b)











# Chess


def chess2colour(chess_board):
    output = []
    for i in range(8):
        l = []
        for j in range(8):
            p = chess_board[i][j]
            if p != 0:
                c=str(p)
                if c.isupper():
                    l.append("blue")
                else:
                    l.append("pink")
#             i = i + 1
            else:
                l.append(0)
        output.append(l)
    return output



def getMove(current_board, new_board):
    old=chess2colour(current_board)
    output = []
    diff = False
    for i in range(8):
        temp = []
        for j in range(8):
            if new_board[i][j] != old[i][j]:
                diff = True
                if new_board[i][j] == 0:
                    old_pos = [i,j]
                    temp.append("t")
                else:
                    new_pos = [i,j]
                    temp.append("t")
            else:
                temp.append(current_board[i][j])
        output.append(temp)
                    

    if diff:
#     Update board
        output[new_pos[0]][new_pos[1]] = current_board[old_pos[0]][old_pos[1]]
        output[old_pos[0]][old_pos[1]] = 0
        return output
    else:
        print("no move made")




def FEN(board, player, halfmove, fullmove):
    output = []
    end_of_zeros = True
    for i in range(8):
        row = []
        zero_counter = 0
        for j in range(8):
            if board[i][j] != 0:
                if not end_of_zeros:
                    row.append(str(zero_counter))
                    zero_counter = 0
                    end_of_zeros = True
                row.append(board[i][j])
            else:
                end_of_zeros = False
                zero_counter = zero_counter + 1
                if j == 7:
                    row.append(str(zero_counter))
                    end_of_zeros = True
        output.append("".join(row))
    config = "/".join(output)
    full = " ".join([config, player, "-", "-", str(halfmove), str(fullmove)])
    return(full)






start_board = [["r","n","b","q","k","b","n","r"],
["p",'p',"p","p","p","p","p","p"],
[0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0],
["P","P","P","P","P","P","P","P"],
["R","N","B","Q","K","B","N","R"]]


chess2colour(start_board)


x = getMove(start_board, b)
pboard(x)


f = FEN(x, 'w', 0, 1)
print(f)


# stockfish engine
sf = Stockfish('/usr/games/stockfish')

sf.set_fen_position(f)
sf.get_best_move()




os.system('rosrun turtlebot_arm_moveit_demos chess_movement.py h1 h2 -1.7 0')
