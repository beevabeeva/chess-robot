#!/usr/bin/env python

import sys
import copy
import rospy
import time
import moveit_commander
import moveit_msgs.msg
from std_msgs.msg import Float64
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from sensor_msgs.msg import JointState
from moveit_commander.conversions import pose_to_list
import sys


def tester():
    gripper = rospy.Publisher('/arm2/gripper_joint/command', Float64, queue_size=10)
    rate = rospy.Rate(1) # 10hz

    i = 0

    while not rospy.is_shutdown():

        
        while i < 3:

            grip = float(sys.argv[1])
            # print(grip)
            gripper.publish(grip)
            i = i + 1
            rate.sleep()


        # gripper.publish(-1.6)
        rate.sleep()
        
        break # that's all folks!



if __name__ == '__main__':

    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('test', anonymous=True)

    try:
        tester()
    except rospy.ROSInterruptException:
        pass



