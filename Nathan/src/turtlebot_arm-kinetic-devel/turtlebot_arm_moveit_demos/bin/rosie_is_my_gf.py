#!/usr/bin/env python

import sys
import copy
import rospy
import time
import math
import moveit_commander
import moveit_msgs.msg
from std_msgs.msg import Float64
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from sensor_msgs.msg import JointState
from moveit_commander.conversions import pose_to_list



def tester():
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    rate = rospy.Rate(1) # 10hz

    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()
    group_name = "arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)
    planning_frame = move_group.get_planning_frame()

    while not rospy.is_shutdown():


        joint_goal = move_group.get_current_joint_values()
        print joint_goal
        rate.sleep()


# # halfway position
        joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()

# right up position
        joint_goal = [0,0,0,0,0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()

        gripper.publish(grip)
        rate.sleep()


        gripper.publish(grip)
        rate.sleep()

# halfway position
        joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()

# down state
        joint_goal = [-1.6106798272799234, -2.1373465644539933, 0.260776733940559, 0.19430423313218123, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()


        # gripper.publish(-1.6)
        rate.sleep()
        
        break # that's all folks!


def upright(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal = [0,0,0,0,0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break


def halfway(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal = [-1.5902267501081149, -0.6135923151542565, -1.2527509767732736, -0.48064731353750095, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break


def downstate(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal =  [1.375469439804125, 0.21987057959694192, -1.927702523442956, -0.8612946270750019, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break


def up1(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal =  [1.3703561705111729, 0.5829126993965437, -1.9021361769781953, -1.0584467436410925, 0.0]



        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break

def move1(move_group):
    rate = rospy.Rate(1) # 10hz
    while not rospy.is_shutdown():
        joint_goal = [1.4010357862688856, -0.015339807878856412, -1.380582709097077, -1.0328803971763318, 0.0]
        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break

def down2(move_group):
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        joint_goal = [1.3959225169759335, -0.21987057959694192, -1.2908092476829814, -1.0175405892974754, 0.0]

        # [1.4266021327336464, -0.39883500485026674, -1.2834305925309866, -1.0277671278833795, 0.0] 
        # [1.4726215563702156, -0.3783819276784582, -1.416375594147742, -0.8641425105089112, 0.0]
        # [1.4266021327336464, -0.409061543436171, -1.2834305925309866, -1.0226538585904275, 0.0]

        move_group.go(joint_goal, wait=True)
        move_group.stop()
        rate.sleep()
        break

def grip(grip1):
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    grip_status = rospy.Subscriber('/joint_states',JointState)
    rate = rospy.Rate(1) # 10hz
    t = 0
    while not rospy.is_shutdown():
        while t < 2:
            gripper.publish(grip1)
            t = t + 1
            rate.sleep()
        print("hello teepa")
        break


def gripKnight():
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    grip_status = rospy.Subscriber('/joint_states',JointState)
    rate = rospy.Rate(1) # 10hz
    grip1=-0.5
    t = 0
    while not rospy.is_shutdown():
        while t < 2:
            gripper.publish(grip1)
            t = t + 1
            rate.sleep()
        print("hello teepa")
        break


def downstate_test(move_group):
    shoulder_lift = rospy.Publisher('/arm_shoulder_lift_joint/command', Float64, queue_size=10)
    rate = rospy.Rate(10) # 10hz
    print("rate" ,rate)
    while not rospy.is_shutdown():
        i = 0
        j=30
        position1 = 0
        while position1 < 0.7:
            position1 = 0.7*math.sin(math.pi*(0.01*i))
            # shoulder_lift.publish(position1)
            position2 = 0.7 - 0.7*math.exp(-i)
            pos3=i*0.7/50
            shoulder_lift.publish(position1)
            print(pos3)
            i = i + 1
            rate.sleep()
            rate = rospy.Rate(j) # 10hz
            print("sleep: ",j )
            j=j-0.5

        break

def release():
    gripper = rospy.Publisher('/gripper_joint/command', Float64, queue_size=10)
    grip_status = rospy.Subscriber('/joint_states',JointState)
    rate = rospy.Rate(1) # 10hz
    grip1=0
    t = 0
    while not rospy.is_shutdown():
        while t < 2:
            gripper.publish(grip1)
            t = t + 1
            rate.sleep()
        print("hello teepa")
        break



if __name__ == '__main__':

    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('test', anonymous=True)
    group_name = "arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    move_group.set_max_velocity_scaling_factor(0.5);
    # move_group.set_max_acceleration_scaling_factor(0.1);

    try:
        # tester()
        # upright(move_group)
        # halfway(move_group)
        # downstate(move_group)
        # halfway(move_group)
        # upright(move_group)
        release()
        up1(move_group)

        downstate(move_group)
        # downstate_test(move_group)
        grip(-1.5)
        up1(move_group)
        move1(move_group)
        # upright(move_group)
        down2(move_group)
        release()
        move1(move_group)
        time.sleep(2)


    except rospy.ROSInterruptException:
        pass





# [1.7333982903107745, 0.6238188537401608, -1.968608677786573, -0.9357282806102412, 0.0]
