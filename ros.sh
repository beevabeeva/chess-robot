#!/bin/bash


SCROLL TO THE BOTTOM IF YOU JUST WANT TO SETUP THE PROJECT!


_______________________________________________________________________

THE STEPS BELOW DESCRIBE HOW WE BUILT THIS PROJECT:


If stuff goes wrong restart roscore

1. source /opt/ros/kinetic/setup.bash
2. mkdir -p ~/pid_ws/src; cd ~/pid_ws/src
3. catkin_create_pkg pid_package std_msgs rospy
4. cd ..; catkin_make
5. source devel/setup.bash
6. roscd pid_package
7. mkdir scripts; cd scripts
a. cd tp src
	wget http://courses.ms.wits.ac.za/robotics2017/tum_simulator.zip; unzip tum_simulator.zip #for tum
b. cd ..
c. catkin_make
d. source devel/setup.bash
8. # make your python scripts and put them in scripts
9. chmod +x *.py
10. roslaunch cvg_sim_gazebo ardrone_testworld.launch

11. see this to get states
		- http://gazebosim.org/tutorials?tut=ros_comm&cat=connect_ros#GetModelStateExample 
		- https://answers.ros.org/question/261782/how-to-use-getmodelstate-service-from-gazebo-in-python/
		
	you can: rostopic echo -n 1 /gazebo/model_states
	or rosservice call gazebo/get_model_state '{model_name: quadrotor}'



ROS Stuff:

1. Unfortunately, all this requires Ubuntu 14 or 16 to work.
 To install ROS kinetic follow the intructions here:
 http://wiki.ros.org/kinetic/Installation/Ubuntu

______________________________
ROS community and packages quirks:
They all host them on github with crap Readmes if you're lucky

1. Download zip of git repo
2. make a new folder for the thing
3. In new folder make another folder called 'src'
4. Extract the package into the src folder you just made.
5. Inside root folder (the one you made in step 2), open terminal and run 'catkin_make'
______________________________

ARM stuff

1. Flash the ros.ino sketch to the robot using arduino : https://answers.ros.org/question/234388/solvedgetting-started-with-arbotix-python-keeping-it-simple/
2. follow these instructions: https://edu.gaitech.hk/turtlebot/turtlebot-arm-pincher.html
2.1 this has to run in the background for anything to work... roslaunch turtlebot_arm_bringup turtlebot_arm_bringup.launch
2.2 * note that although arbotix_driver seems to do the same as the command in 2.1, it does not provide all the rostopics needed for the motors.
3. https://github.com/turtlebot/turtlebot_arm
4. rostopic publisher release does not work for some mysterious reason... (cuuuuuuuute little teep)
	4.1 Its because there is no information about the values that `/gripper_joint/command` can take ([-2.5:0]).
5. Joint Values
	- 5 (gripper_joint): [-2.5:0]
	- 4 (wrist_flex): [-1:3]
	- 1 (shoulder_pan): [-4:5]
	- 2 (shoulder_lift): [-2.5: 2.5]
	- 3 (elbow_flex): [-2.5 : 2.5]

______________________________
ARM Simulation stuff
1. Gazebo: 
	- https://answers.ros.org/question/228576/how-can-turtlebot-be-simulated-with-turtlebot-arm-package/
	- http://answers.gazebosim.org/question/18994/turtlebotpincher-arm-gazebo-simulation-with-moveit/
	- https://github.com/joellutz/pincher_arm_test
	- https://github.com/corot/thorp
	- https://answers.ros.org/question/201625/turtlebot-arm-on-gazebo-lets-fall-all-grasped-objects/
	- https://github.com/kbogert/phantomx_arm
	
______________________________
Two ARMS:
>http://answers.ros.org/question/143296/how-to-set-port-devttyusb-parameter-in-arbotix-python/
>http://answers.ros.org/question/282060/how-to-have-multi-turtlebot-3-in-a-real-environment/

1.  In one terminal:
	 > in turtlebot_arm (after sourceing devel..) export ROS_NAMESPACE=arm1
	 > rosrun arbotix_python arbotix_driver _port:=/dev/ttyUSB1
2.  In another terminal:
	 > in turtlebot_arm (after sourceing devel..) export ROS_NAMESPACE=arm2
	 > rosrun arbotix_python arbotix_driver _port:=/dev/ttyUSB0
3. Check: (new terminal) rostopic list: should see arm1 and arm2 commands.

4. Do this  (2 files per arm) for bringup.launch: (https://riptutorial.com/ros/example/24423/launch-ros-nodes-and-load-parameters-from-yaml-file)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		<launch>
	  <!-- To use, first set Environment variable TURTLEBOT_ARM1 to either:
	   turtlebot or pincher (for Trossen PhantomX Pincher)
	   NOTE: passing arm_type as argument NOT yet fully supported! -->

	  <arg name="simulation" default="false"/>
	  <arg name="arm_type" default="$(optenv TURTLEBOT_ARM1 turtlebot)"/>

	  <param name="robot_description" command="$(find xacro)/xacro --inorder '$(find turtlebot_arm_description)/urdf/$(arg arm_type)_arm.urdf.xacro'"/>
	  <node name="robot_state_pub" pkg="robot_state_publisher" type="robot_state_publisher"/>
	  <node name="fake_joint_pub" pkg="turtlebot_arm_bringup" type="fake_joint_pub.py"/>

	  <node name="arbotix" pkg="arbotix_python" type="arbotix_driver" output="screen">
	    <param name="port" value="/dev/ttyUSB1" />
	    <rosparam file="$(find turtlebot_arm_bringup)/config/$(arg arm_type)_arm.yaml" command="load"/>
	    <param name="sim" value="$(arg simulation)"/>
	  </node>

	  <node name="gripper_controller" pkg="arbotix_controllers" type="gripper_controller" output="screen">
	    <rosparam file="$(find turtlebot_arm_bringup)/config/$(arg arm_type)_gripper.yaml" command="load" />
	  </node>
	</launch>

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
______________________________
MoveIt!

1. https://github.com/ros-planning/moveit/tree/kinetic-devel:
sudo apt-get install ros-kinetic-moveit

2. in moveit_commander folder: python setup.py install
3. http://answers.ros.org/question/87451/setting-moveit-goal-state-in-rviz/
4. https://www.youtube.com/watch?v=d3nj_s6KD4Q
5. follow nathans installs: 
6  https://github.com/ros-planning/moveit_tutorials/blob/kinetic-devel/doc/move_group_python_interface/scripts/move_group_python_interface_tutorial.py
7. TODO: try moveit with database:  https://www.youtube.com/watch?v=d3nj_s6KD4Q 7:40


8. Abort error during path planning/execution:

	https://github.com/qboticslabs/mastering_ros/issues/24
	You should add the following parameter "allowed_start_tolerance" in trajectory_execution.launch.xml:
 	 <param name="trajectory_execution/allowed_start_tolerance" value="0.0"/>

9. export TURTLEBOT_ARM1=pincher
10. roslaunch turtlebot_arm_moveit_config turtlebot_arm_moveit.launch sim:=false --screen


______________________________
IMAGE stuff
	
mainly:
	>export TURTLEBOT_3D_SENSOR=kinect (for all terminals I think)
	>roslaunch freenect_launch freenect.launch
	>rosrun rviz rviz 
		>Panels -> camera
		> Display -> set 'Image Topic' to `/camera/rgb/image_raw`
		> color:  set 'Transparent Hint' to `theora`
		> will probably need to resize the camera pane but it is there...



- https://answers.ros.org/question/210294/ros-python-save-snapshot-from-camera/
- USB cam: https://www.youtube.com/watch?v=StHfHCPrHOw
- Kinect
	- https://www.youtube.com/watch?v=cjC5tXpVXzE
	- setup: http://wiki.ros.org/Robots/evarobot/Tutorials/indigo/Kinect
		- Note: for jsdl error: https://askubuntu.com/questions/883436/openjdk-7-jdk-package/883459
	- https://answers.ros.org/question/196455/kinect-installation-and-setup-on-ros-updated/
	To see if its working:
	- https://www.youtube.com/watch?v=9xN7zRUZsIw

- Kinect in RVIZ:
	- https://www.youtube.com/watch?v=cjC5tXpVXzE
	- In 'Display' panel -> 'camera' (seen after adding a camera thing as in the video above...) -> Set 'Image Topic' to `/camera/rgb/image_raw` or any other relevant topic...
		- to get color, set 'Transparent Hint' to `theora`

	- Calibrate checkerboard (from this guys tut:https://github.com/IOJVision/PhantomX-Pincher-Vibot-2019-/blob/master/readme.md)
		- Will not because the environment thinks we're using an Asus cam...'
		- change this by doing this: `export TURTLEBOT_3D_SENSOR=kinect`. (answer from obscure chinese website:https://www.ncnynl.com/archives/201609/810.html)
		- put this in .bashrc 

- NB: We must run `roslaunch freenect_launch freenect.launch` for ROS to interact with the Kinect.

______________________________

Web cam
1. vlc v412:/dev/video0
2. Opencv and ros: https://github.com/ros-drivers/video_stream_opencv
3. check vlc vido toools: awesome, colour stuff, motion detection!
______________________________

CHESS stuff

- Tracking pieces via starting positions and changes: https://www.youtube.com/watch?v=HTcd-0S1c3c
- CNN chamfer matching: https://www.youtube.com/watch?v=0BjEKT-tfdw
- a blog about it: https://medium.com/@daylenyang/building-chess-id-99afa57326cd
- a paper on doing it complexly: https://web.stanford.edu/class/ee368/Project_Spring_1415/Reports/Danner_Kafafy.pdf




_______________________________________________________________________
_______________________________________________________________________
_______________________________________________________________________
_______________________________________________________________________
_______________________________________________________________________


Project Start__________________________________________________________________________________
1. roscore
2. (new terminal) cd /home/robotics/turtlebot_arm
*3.1  sudo rosdep install --from-paths -r src --rosdistro kinetic
You have to do the above. Do not use -y. This is a new thing. It is stupid.
3.1 catkin_make
4. source devel/setup.bash
4.1 roscd turtlebot_arm_bringup
5. roslaunch turtlebot_arm_bringup arm1.launch
	(new terminal): roslaunch turtlebot_arm_bringup arm2.launch
if that doesnt work
roslaunch /home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/src/turtlebot_arm-kinetic-devel/turtlebot_arm_bringup/launch/arm1.launch
6. (new terminal)  source devel/setup.bash
7. roscd turtlebot_arm_bringup/
8. rosrun turtleboarm_bringup turtle.py 
9. (new terminal) roslaunch freenect_launch freenect.launch
10. check both things working then terminate freenect_launch and  turtleboarm_bringup
11. do calibration https://github.com/IOJVision/PhantomX-Pincher-Vibot-2019-/blob/master/readme.md (see 'IMAGE stuff')
12. 



* After set up:
RUN: python /home/chess/Documents/Chess_Robot/Avi_Niambh/chess-robot/Nathan/src/turtlebot_arm-kinetic-devel/turtlebot_arm_moveit_demos/gameclock.py



______________________________

USB Shit
https://unix.stackexchange.com/questions/66901/how-to-bind-usb-device-under-a-static-name doesnt works




______________________________
pip Shit (for gameclock)
sudo apt install gameclock
pip(2) install pynput

Opencv:
 > pip3 install opencv-python 

 > use like this:
===========
ros_path = '/opt/ros/kinetic/lib/python2.7/dist-packages/cv2.so'
if ros_path or nathan_path in sys.path:

    sys.path.remove(ros_path)

import cv2

sys.path.append('/opt/ros/kinetic/lib/python2.7/dist-packages/cv2.so')
=============
If tha does not work,
> remove cv2.so in  /opt/ros/kinetic/lib/python2.7/dist-packages/cv2.so
That seemed to work. I did not remove the ros_path = ... story in cv_chess.py either.

Skimage:
	pip3 install --upgrade pip==9.0.1
	pip3 install scikit-image --user

	

tk_inter:
	sudo apt-get install python3-tk

stockfish;
	(engine): sudo apt-get install stockfish 
	(python): pip3 install stockfish
